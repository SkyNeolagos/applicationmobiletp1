package com.example.applicationmobiletp1;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        if(name !=null){
            //Pointe vers l'animal sélectionné
            final Animal animalCurrent=AnimalList.getAnimal(name);

            //Set nom
            TextView textName=findViewById(R.id.nameAnimal);
            textName.setText(name);

            //Set image
            ImageView imgAnimal = (ImageView) findViewById(R.id.animalImage);
            int resId=getResources().getIdentifier(animalCurrent.getImgFile(),"drawable",getPackageName());
            imgAnimal.setImageResource(resId);

            //Set vie
            TextView textVie=findViewById(R.id.animalVieInfo);
            textVie.setText(animalCurrent.getStrHightestLifespan());

            //Set Poids naissance
            TextView textPoidsNaissance=findViewById(R.id.animalPoidsInfo);
            textPoidsNaissance.setText(animalCurrent.getStrBirthWeight());

            //Set Poids adulte
            TextView textPoidsAdulte=findViewById(R.id.animalPoidsAdulteInfo);
            textPoidsAdulte.setText(animalCurrent.getStrAdultWeight());

            //Set Gestation
            TextView textGestation=findViewById(R.id.animalGestationInfo);
            textGestation.setText(animalCurrent.getStrGestationPeriod());

            //Set Conservation
            final TextView textConservation=findViewById(R.id.animalStatutInfo);
            textConservation.setText(animalCurrent.getConservationStatus());

            final Button animalBouton =findViewById(R.id.animalSave);
            animalBouton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animalCurrent.setConservationStatus(textConservation.getText().toString());
                }
            });
        }
    }
}
