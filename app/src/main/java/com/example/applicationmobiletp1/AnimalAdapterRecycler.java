package com.example.applicationmobiletp1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AnimalAdapterRecycler extends RecyclerView.Adapter<AnimalAdapterRecycler.ViewHolder>  {
    private String[] animaux;

    AnimalAdapterRecycler(String[] animaux){
        this.animaux=animaux;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.animal_item_recycle,parent,false);
        return new ViewHolder(view,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.display(animaux[position]);
    }

    @Override
    public int getItemCount() {
        return animaux.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView animalNameViewHolder;
        private ImageView animalIconViewHolder;
        private Context context;

        public ViewHolder(View itemView,Context context) {
            super(itemView);
            animalNameViewHolder=(TextView)itemView.findViewById(R.id.animalNameRecycle);
            animalIconViewHolder= (ImageView) itemView.findViewById(R.id.animalIconRecycle);
            this.context=context;
        }
        void display(String name){
            animalNameViewHolder.setText(name);
            final Animal animalCurrent=AnimalList.getAnimal(name);
            int resId=context.getResources().getIdentifier(animalCurrent.getImgFile(),"drawable",context.getPackageName());
            animalIconViewHolder.setImageResource(resId);
        }
    }
}

