package com.example.applicationmobiletp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;



public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Liste fixe des animaux
        final String[] animaux= AnimalList.getNameArray();
        //Recuperer la RecyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.zooListView);
        AnimalAdapterRecycler animalAdapterRecycler=new AnimalAdapterRecycler(animaux);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(animalAdapterRecycler);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this.getBaseContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Intent intent= new Intent(MainActivity.this,AnimalActivity.class);
                        intent.putExtra("name",animaux[position]);
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }
}